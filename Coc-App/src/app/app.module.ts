import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { HeaderComponent }  from './header.component';
import { DashboardComponent }  from './dashboard.component';
import { SpellDetailComponent }  from './spell-detail.component';
import { HeroDetailComponent }  from './hero-detail.component';

import {AppService} from './app.service';

@NgModule({
  imports:      [ BrowserModule,FormsModule ],
  declarations: [ AppComponent,HeaderComponent,DashboardComponent,SpellDetailComponent,HeroDetailComponent],
  providers:[AppService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
