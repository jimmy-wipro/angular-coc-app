import { Component, Input } from '@angular/core';
import { Hero } from './Hero';

@Component({
    selector: 'hero-detail',
    template: `
   <div class="card" style="width: 20rem;" *ngIf="hero">
        <div class="card-block">
          <h4 class="card-title"> {{hero.name}}</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        </div>
   </div>

    `
})
export class HeroDetailComponent {
    constructor() {
        console.log(this.hero)
    }
    @Input() hero: Hero;

}