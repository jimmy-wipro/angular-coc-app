import { Component, Input } from '@angular/core';
import { Spell } from './spell';

@Component({
    selector: 'spell-detail',
    template: `
      <div class="card" style="width: 20rem;" *ngIf="spell">
        <div class="card-block">
          <h4 class="card-title"> {{spell.name}}</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        </div>
   </div>
    `
})
export class SpellDetailComponent {
    constructor() {
        console.log(this.spell)
    }
    @Input() spell: Spell;

}