import { Component, Input } from '@angular/core';
import { AppService } from './app.service';
import { Hero } from './hero';
import { Troop } from './troop';
import { Building } from './building';
import { Spell } from './spell';

@Component({
  selector: 'dash',
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent {
  constructor(private appService: AppService) { }
  categories = this.appService.getCategories();
  heroes = this.appService.getHeroes();
  spells = this.appService.getSpells();
  selectedSpell: Spell;
  selectedHero: Hero;
  onSelectSpell(spell: Spell) {
    this.selectedSpell = spell;
    this.selectedHero = null;;
  }
  onSelectHero(hero: Hero) {
    this.selectedHero = hero;
    this.selectedSpell = null;;
  }
}