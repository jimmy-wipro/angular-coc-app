"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
exports.CATEGORIES = [
    { id: 1, name: 'Heroes' },
    { id: 2, name: 'Troops' },
    { id: 3, name: 'Spells' },
    { id: 4, name: 'Buildings' }
];
exports.HEROES = [
    { id: 1, name: 'Barbarian King' },
    { id: 2, name: 'Archer Queen' }
];
exports.SPELLS = [
    { id: 1, name: 'Healing Spell' },
    { id: 2, name: 'Raging Spell' },
    { id: 3, name: 'Lightening Spell' }
];
var AppService = (function () {
    function AppService() {
    }
    AppService.prototype.getCategories = function () {
        return exports.CATEGORIES;
    };
    AppService.prototype.getHeroes = function () {
        return exports.HEROES;
    };
    AppService.prototype.getSpells = function () {
        return exports.SPELLS;
    };
    return AppService;
}());
AppService = __decorate([
    core_1.Injectable()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map