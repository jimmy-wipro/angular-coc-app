import {Injectable} from '@angular/core';
import {Hero} from './hero';
import {Troop} from './troop';
import {Building} from './building';
import {Spell} from './spell';

export const CATEGORIES=[

    { id: 1, name: 'Heroes' },
    { id: 2, name: 'Troops' },
    { id: 3, name: 'Spells' },
    { id: 4, name: 'Buildings' }
];
export const HEROES:Hero[]=[
    { id: 1, name: 'Barbarian King' },
    { id: 2, name: 'Archer Queen' }

];
export const SPELLS:Spell[]=[
    { id: 1, name: 'Healing Spell' },
    { id: 2, name: 'Raging Spell' },
    { id: 3, name: 'Lightening Spell' }

];

@Injectable()
export class AppService{
getCategories(){
    return CATEGORIES;
}
getHeroes(){
return HEROES;
}

getSpells(){
    return SPELLS;
}
}