import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<div class="container-fluid">
              <h2>Hello {{name}} </h2>
              <header></header>
               <dash></dash>
             </div>
             `,
})
export class AppComponent  { name = 'Clashers'; }
